import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
* Test of Turtle class. 
* 
* @author (Francisco Guerra) 
* @version (Version 1)
*/
@RunWith(GreenfootRunner.class)
public class TurtleTest {

    Board  board;	

    @Before
    public void setUp() throws Exception {
        board  = new Board();
    }

    @Test
    public void testImage() throws Exception {
        // Given
        Turtle turtle = new Turtle();

        // When
        GreenfootImage image = turtle.getImage();
        
        // Then
		assertEquals("turtle.png", image.getImageFileName());
    }

    @Test
    public void testImageWidth() throws Exception {
        // Given
        Turtle turtle = new Turtle();

        // When
        GreenfootImage image = turtle.getImage();
        
        // Then
		assertEquals(50, image.getWidth());
    }

    @Test
    public void testImageHeight() throws Exception {
        // Given
        Turtle turtle = new Turtle();

        // When
        GreenfootImage image = turtle.getImage();
        
        // Then
		assertEquals(40, image.getHeight());
    }

    @Test
    public void testTurtleAct_X() throws Exception {
        // Given

        // When

        // Then
		fail("Not yet implemented");
    }

    @Test
    public void testTurtleAct_Y() throws Exception {
        // Given

        // When

        // Then
		fail("Not yet implemented");
    }

    @Test
    public void testSetAngle180() throws Exception {
        // Given

        // When

        // Then
		fail("Not yet implemented");
    }
    
    @Test
    public void testSetAngle270() throws Exception {
        // Given

        // When

        // Then
		fail("Not yet implemented");
    }
    
    @Test
    public void testSetDistance5Angle0() throws Exception {
        // Given

        // When

        // Then
		fail("Not yet implemented");
    }

    @Test
    public void testSetDistance3Angle90() throws Exception {
        // Given

        // When

        // Then
		fail("Not yet implemented");
    }
    
}
