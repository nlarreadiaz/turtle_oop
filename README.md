Created by Francisco Guerra (francisco.guerra@ulpgc.es) for teaching.

This  eclipse project  duplicates  several directories  and  files  to 
facilitate its execution by means of two tools: GREENFOOT and ECLIPSE.

The main goal  of this duplication is to facilitate teaching using the 
advantages of each tool: the main advantage of GREENFOOT is simplicity 
of use,  and the main advantage  of ECLIPSE is to become familiar with 
Test Driven Development (TDD).

There are two run configurations: 
  - GreenfootScenarioMain (to run with ECLIPSE), and 
  - RunGreenfootFromEclipse (to run with GREENFOOT).

Eclipse does not know Greenfoot and therefore does not immediately detect 
the changes  Greenfoot makes.  So it’s  very important  to press  F5 when 
Greenfoot IDE ends and we go back to Eclipse IDE.
